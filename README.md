### ENCORA Training Basics

Decorator Design

# What is the Decorator Design Pattern?

**Decorator** is a structural design pattern that lets you *attach new behaviors to objects by placing* these objects inside special wrapper objects that contain the behaviors.

> You can find more information in [this link](https://refactoring.guru/design-patterns/decorator)!
>
> # Problem
> Imagine that you have to buy a **SANDWICH** but you
> see something extra to put on, but you already order it.
> so there is real problem to solve :(.

![Decorator Design Pattern](https://refactoring.guru/images/patterns/content/decorator/decorator.png?id=710c66670c7123e0928d3b3758aea79e)


## Strongest skills 

- The Decorator Allows you to modify an object dynamically.
- You would use it when you want the capabilities of inheritance with subclasses, but you need to add functionality at run time.
- It is more flexible than inheritance.
- Simplifies code because you add functionality using many simple classes.
- Rather than rewrite old code you can extend with new code.

### Talking about the code

In this code on python has been demonstrated the Decorator Design that has been made on the following classes:

----------------
> ## Parent Class
> Sandwich

> ## Kind of Sandwiches
> **Subway Club**

> **Steak And Cheese**

> **Chicken And Bacon Ranch**

> ## Sandwich Decorator
> A class for united the extras you want to add

> ## Extras
> **Avocado**

> **Bacon**

## Run code

In order to run the code you'll need installed the last version of [python](https://www.python.org/downloads/)

Once you have it you'll need to open your favorite terminal and navigate to the file location.

Once you are located in the file location you need to run the next code:

```
py DecoratorPattern.py
```