"""
What is the Decorator Design Pattern?

- The Decorator Allows you to modify an object dynamically.
- You would use it when you want the capabilities of inheritance with subclasses, but you need to add functionality at run time.
- It is more flexible than inheritance.
- Simplifies code because you add funtionality using many simple classes.
- Rather than rewrite old code you can extend with new code.

We have a Sandwich but we whant add extras to it
"""

class Sandwich:
	description = "Unknown Sandwich"

	def cost(self):
		raise NotImplementedError()

	def get_description(self):
		return self.description


class SubwayClub(Sandwich):
	def __init__(self) -> None:
		self.description = "Subway Club"

	def cost(self):
		return 7.99

class SteakAndCheese(Sandwich):
	def __init__(self) -> None:
		self.description = "Steak And Cheese"

	def cost(self):
		return 8.99

class ChickenAndBaconRanch(Sandwich):
	def __init__(self) -> None:
		self.description = "Chicken And Bacon Ranch"

	def cost(self):
		return 9.99

class SandwichDecorator(Sandwich):
	def __init__(self, sandwich: Sandwich) -> None:
		self.sandwich = sandwich

	def get_description(self):
		raise NotImplementedError()

class Avocado(SandwichDecorator):
	def get_description(self):
		return self.sandwich.get_description() + ", Avocado"

	def cost(self):
		return self.sandwich.cost() + 1.29

class Bacon(SandwichDecorator):
	def get_description(self):
		return self.sandwich.get_description() + ", Bacon"

	def cost(self):
		return self.sandwich.cost() + 0.99

if __name__ == "__main__":
	club = SubwayClub()
	print("Your choice is: " + club.get_description() + ". It has a cost of: " + str(club.cost()))

	chicken_and_bacon = SubwayClub()
	chicken_and_bacon = Avocado(chicken_and_bacon)
	chicken_and_bacon = Bacon(chicken_and_bacon)

	print("Final Order: " + chicken_and_bacon.get_description())
	print("Total: $"+ str(chicken_and_bacon.cost()))